#!/bin/sh
# Once you've built all the packages, run this script over /var/cache/pbuilder/result
sed -i -e '/^Distribution:/ { /Distribution: unstable/ b fwakata s/$/-backport/; :fwakata }' *.changes
