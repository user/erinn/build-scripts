###
### Makefile for building Tor & Vidalia bundles on Mac OS X
###
### Copyright 2007 Steven J. Murdoch <http://www.cl.cam.ac.uk/users/sjm217/>
### Copyright 2009, 2010 Jacob Appelbaum <jacob@appelbaum.net>
### Copyright 2010 Erinn Clark <erinn@torproject.org>

#####################
### Configuration ###
#####################

## Location of directory for source unpacking
FETCH_DIR=$(PWD)
## Location of directory for prefix/destdir/compiles/etc
BUILT_DIR=$(FETCH_DIR)/built
DMG_DIR=$(FETCH_DIR)/dmgs
ARCH_TYPE=$(shell arch)

## Versions for our source packages
VIDALIA_VER=0.2.14
LIBEVENT_VER=2.0.13-stable
TOR_VER=0.2.2.32
OPENSSL_VER=0.9.8p
POLIPO_VER=1.0.4.1
TORBUTTON_VER=1.4.0
ZLIB_VER=1.2.5

## Extension IDs
#FF_VENDOR_ID:=\{ec8030f7-c20a-464f-9b0e-13a3a9e97384\}

## File names for the source packages
VIDALIA_PACKAGE=vidalia-$(VIDALIA_VER).tar.gz
LIBEVENT_PACKAGE=libevent-$(LIBEVENT_VER).tar.gz
TOR_PACKAGE=tor-$(TOR_VER).tar.gz
POLIPO_PACKAGE=polipo-$(POLIPO_VER)-with-mods.tar.gz
ZLIB_PACKAGE=zlib-$(ZLIB_VER).tar.gz
OPENSSL_PACKAGE=openssl-$(OPENSSL_VER).tar.gz

## Location of files for download
VIDALIA_URL=http://www.torproject.org/dist/vidalia/$(VIDALIA_PACKAGE)
LIBEVENT_URL=http://www.monkey.org/~provos/$(LIBEVENT_PACKAGE)
TOR_URL=http://www.torproject.org/dist/$(TOR_PACKAGE)
POLIPO_URL=http://interloper.org/tmp/polipo/$(POLIPO_PACKAGE)
ZLIB_URL=http://www.gzip.org/zlib/$(ZLIB_PACKAGE)
OPENSSL_URL=http://www.openssl.org/source/$(OPENSSL_PACKAGE)

fetch-source:
	-mkdir $(FETCH_DIR)
	$(CURL) $(VIDALIA_URL) --O $(FETCH_DIR)/$(VIDALIA_PACKAGE)
	$(CURL) $(LIBEVENT_URL) --O $(FETCH_DIR)/$(LIBEVENT_PACKAGE)
	$(CURL) $(TOR_URL) --O $(FETCH_DIR)/$(TOR_PACKAGE)
	$(CURL) $(POLIPO_URL) --O $(FETCH_DIR)/$(POLIPO_PACKAGE)

unpack-source:
	cd $(FETCH_DIR) && tar -xvzf $(VIDALIA_PACKAGE)
	cd $(FETCH_DIR) && tar -xvzf $(LIBEVENT_PACKAGE)
	cd $(FETCH_DIR) && tar -xvzf $(TOR_PACKAGE)
	cd $(FETCH_DIR) && tar -xvzf $(POLIPO_PACKAGE)

source-dance: torbutton.xpi fetch-source unpack-source
	echo "We're ready for building now."

ZLIB_DIR=$(FETCH_DIR)/zlib-$(ZLIB_VER)
ZLIB_OPTS=--prefix=$(BUILT_DIR)
ZLIB_CFLAGS="-mmacosx-version-min=10.4 -isysroot /Developer/SDKs/MacOSX10.4u.sdk -arch $(ARCH_TYPE)"
ZLIB_LDFLAGS="-Wl,-syslibroot,/Developer/SDKs/MacOSX10.4u.sdk"
build-zlib:
	cd $(ZLIB_DIR) && CFLAGS=$(ZLIB_CFLAGS) LDFLAGS=$(ZLIB_LDFLAGS) ./configure $(ZLIB_OPTS)
	cd $(ZLIB_DIR) && make
	cd $(ZLIB_DIR) && make install

VIDALIA_DIR=$(FETCH_DIR)/vidalia-$(VIDALIA_VER)
VIDALIA_OPTS=-DOSX_TIGER_COMPAT=1 -DCMAKE_OSX_ARCHITECTURES=$(ARCH_TYPE) -DQT_QMAKE_EXECUTABLE=/usr/bin/qmake \
	-DTOR_SOURCE_DIR=$(TOR_DIR) -DPOLIPO_SOURCE_DIR=$(POLIPO_DIR) -DTORBUTTON_XPI=$(FETCH_DIR)/torbutton.xpi ..
build-vidalia:
	export MACOSX_DEPLOYMENT_TARGET=10.4
	-mkdir $(VIDALIA_DIR)/build
	cd $(VIDALIA_DIR)/build && cmake $(VIDALIA_OPTS)

OPENSSL_DIR=$(FETCH_DIR)/openssl-$(OPENSSL_VER)
OPENSSL_OPTS=-no-idea -no-rc5 -no-md2 no-shared zlib -mmacosx-version-min=10.4 -isysroot /Developer/SDKs/MacOSX10.4u.sdk -Wl,-syslibroot,/Developer/SDKs/MacOSX10.4u.sdk --prefix=$(BUILT_DIR) --openssldir=$(BUILT_DIR) -I$(BUILT_DIR)/lib
build-openssl:
	cd $(OPENSSL_DIR) && ./config $(OPENSSL_OPTS)
	cd $(OPENSSL_DIR) && make depend
	cd $(OPENSSL_DIR) && make
	cd $(OPENSSL_DIR) && make install

LIBEVENT_DIR=$(FETCH_DIR)/libevent-$(LIBEVENT_VER)
LIBEVENT_CFLAGS="-I$(BUILT_DIR)/include -O -g -mmacosx-version-min=10.4 -isysroot /Developer/SDKs/MacOSX10.4u.sdk -arch $(ARCH_TYPE)"
LIBEVENT_LDFLAGS="-L$(BUILT_DIR)/lib -Wl,-syslibroot,/Developer/SDKs/MacOSX10.4u.sdk"
LIBEVENT_OPTS=--prefix=$(BUILT_DIR) --enable-static --disable-shared --disable-dependency-tracking
build-libevent:
	cd $(LIBEVENT_DIR) && CFLAGS=$(LIBEVENT_CFLAGS) LDFLAGS=$(LIBEVENT_LDFLAGS) ./configure $(LIBEVENT_OPTS)
	cd $(LIBEVENT_DIR) && make -j2
	cd $(LIBEVENT_DIR) && sudo make install

TOR_DIR=$(FETCH_DIR)/tor-$(TOR_VER)
TOR_CFLAGS="-I$(BUILT_DIR)/include -O -g -mmacosx-version-min=10.4 -isysroot /Developer/SDKs/MacOSX10.4u.sdk -arch $(ARCH_TYPE)"
TOR_LDFLAGS="-L$(BUILT_DIR)/lib -Wl,-syslibroot,/Developer/SDKs/MacOSX10.4u.sdk"
TOR_OPTS_VIDALIA=--enable-static-libevent --enable-static-openssl --enable-static-zlib --with-libevent-dir=$(BUILT_DIR)/lib \
	--with-openssl-dir=$(BUILT_DIR)/lib --with-zlib-dir=$(BUILT_DIR)/lib --prefix=/Applications/Vidalia.app \
	--bindir=/Applications/Vidalia.app --sysconfdir=/Applications/Vidalia.app --disable-dependency-tracking
build-tor-vidalia:
	cd $(FETCH_DIR) && rm -rf $(TOR_DIR)
	cd $(FETCH_DIR) && tar -xvzf $(FETCH_DIR)/$(TOR_PACKAGE)
	cd $(TOR_DIR) && CFLAGS=$(TOR_CFLAGS) LDFLAGS=$(TOR_LDFLAGS) ./configure $(TOR_OPTS_VIDALIA)
	cd $(TOR_DIR) && make


## Polipo doesn't use autoconf, so we just have to hack their Makefile
## This probably needs to be updated if Polipo ever updates their Makefile
POLIPO_DIR=$(FETCH_DIR)/polipo-$(POLIPO_VER)
build-polipo:
	cd $(POLIPO_DIR) && make CFLAGS="-mmacosx-version-min=10.4 -isysroot /Developer/SDKs/MacOSX10.4u.sdk -arch $(ARCH_TYPE)" LDFLAGS="-Wl,-syslibroot,/Developer/SDKs/MacOSX10.4u.sdk" \
	&& PREFIX=$(FETCH_DIR)/built/ sudo make install 

## Build targets 
build-vidalia-binaries: build-zlib build-openssl build-libevent build-polipo build-tor-vidalia build-vidalia

all: dist-osx-vidalia-bundle dist-osx-split-vidalia-bundle

dist-osx-vidalia-bundle: build-vidalia-binaries
	-mkdir $(DMG_DIR)
	cd $(VIDALIA_DIR)/build && make dist-osx-bundle
	cp $(VIDALIA_DIR)/build/*dmg $(DMG_DIR)

dist-osx-split-vidalia-bundle: build-vidalia-binaries
	-mkdir $(DMG_DIR)
	cd $(VIDALIA_DIR)/build && make dist-osx-split-bundle
	cp $(VIDALIA_DIR)/build/*dmg* $(DMG_DIR)

## Location of compiled libraries
COMPILED_LIBS=$(BUILT_DIR)/lib
## Location of compiled binaries
COMPILED_BINS=$(BUILT_DIR)/bin/

## Location of the libraries we've built
LIBEVENT=$(COMPILED_LIBS)

## Location of utility applications
WGET:=$(shell which wget)
CURL:=$(shell which curl)

DEFAULT_EXTENSIONS=torbutton.xpi

## Where to download Torbutton from
TORBUTTON=http://www.torproject.org/torbutton/releases/torbutton-$(TORBUTTON_VER).xpi

##
## Cleanup
##

clean:
	rm -fr $(DISTDIR)
	rm -fr *.app
	rm -fr $(DEST) *.stamp
	rm -f *~
	rm -fr *.xpi *.jar *.zip
	rm -fr $(NAME)_*
	cd ../src/RelativeLink/ && $(MAKE) clean

## Torbutton development version
torbutton.xpi:
	$(CURL) $(TORBUTTON) --O torbutton.xpi


